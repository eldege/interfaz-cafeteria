package com.mycompany.cafe;

import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.QuadCurve;
import javafx.scene.text.Text;

public class PrimaryController implements Initializable {
    
    

    @FXML
    private TextField txtSaldo;

    @FXML
    private Button btnrecargar;

    @FXML
    private RadioButton btnCapu;

    @FXML
    private RadioButton btnLatte;
    
    @FXML
    private TextField txtRecargarCantidad;

    @FXML
    private RadioButton btnCortado;

    @FXML
    private ComboBox btnTamanio;

    @FXML
    private TextField txtCantidad;

    @FXML
    private Button btnMas;
    
    @FXML
    private QuadCurve curva2;
    
    @FXML
    private QuadCurve curva1;

    @FXML
    private Button btnMenos;

    @FXML
    private TableView tabla;
    
    @FXML
    private TableColumn colTipo;

    @FXML
    private TableColumn colTamanio;

    @FXML
    private TableColumn colCantidad;

    @FXML
    private TableColumn colPrecio;
    
    @FXML
    private ColorPicker btnColor;

    @FXML
    private TextField txtPrecio;
    
    @FXML
    private Text titulo2;
    
    @FXML
    private Text titulo1;
    
    @FXML
    private TextField txtBuscarTipoCafé;

    @FXML
    private Button btnPedir;
    
    private double saldo = 0;
    
    private double precioTotal = 0.0;

    @FXML
    private Button btnCancelar;
    
    private ObservableList<Café>cafés;
    
    private ObservableList<Café> filtroCafés;
    
    private ToggleGroup tg;
    
    @FXML
    public void increase(ActionEvent event){
        int resultado = Integer.parseInt(txtCantidad.getText());
        resultado=resultado+1;
        txtCantidad.setText(Integer.toString(resultado));
         }
   
    @FXML
    public void decrease(ActionEvent event) {
    int resultado = Integer.parseInt(txtCantidad.getText());
    

    if (resultado > 0) {
        resultado = resultado - 1;
        txtCantidad.setText(Integer.toString(resultado));
    } else {
        Alert alerta = new Alert(Alert.AlertType.ERROR);
        alerta.setTitle("Error");
        alerta.setContentText("La cantidad no puede ser negativa.");
        alerta.showAndWait();
    }
}
    
    @FXML
void cambiarColor(ActionEvent event) {
    Color nuevoColor = btnColor.getValue();
    curva1.setFill(nuevoColor);
    curva2.setFill(nuevoColor);
    
}

@FXML
void colorOriginal (ActionEvent event) {
    curva1.setFill(Color.rgb(158, 97, 69));
    curva2.setFill(Color.rgb(158, 97, 69));
    btnColor.setValue(Color.rgb(158, 97, 69));
}


    @FXML
    public void buscarCafesPorTipo(ActionEvent event) {
            String filtro = this.txtBuscarTipoCafé.getText().toLowerCase();

        if (filtro.isEmpty()){
            this.tabla.setItems(cafés);
        }else {
            this.filtroCafés.clear();
            for (Café c : this.cafés){
                if (c.getTipo().toLowerCase().contains(filtro)){
                    this.filtroCafés.add(c);
                }
            }

            tabla.setItems(filtroCafés);
        }
}
    
    
    @FXML
    void agregarCafé(ActionEvent event) {
    try {
        String tipo = null;
        
        
        
        if (btnCapu.isSelected()) {
            tipo = "Capuccino";
        } else if (btnLatte.isSelected()) {
            tipo = "Latte";
        } else if (btnCortado.isSelected()) {
            tipo = "Cortado";
        }
        
        String tamaño = (String) btnTamanio.getValue();
        int cantidad = Integer.parseInt(txtCantidad.getText());
        
        if (cantidad < 0) {
            Alert alerta = new Alert(Alert.AlertType.ERROR);
            alerta.setTitle("Error");
            alerta.setContentText("La cantidad no puede ser negativa.");
            alerta.showAndWait();
            return;
        }
        
        double precio = 0.0;
        
        if (tamaño.equals("Pequeño")) {
            precio = 1.0;
        } else if (tamaño.equals("Mediano")) {
            precio = 1.5;
        } else if (tamaño.equals("Grande")) {
            precio = 2.0;
        }
        
        precio *= cantidad;
        
        
        if (saldo >= precio) {
            Café c = new Café(tipo, tamaño, cantidad, precio);
            cafés.add(c);
            tabla.setItems(cafés);
            
            precioTotal += precio;
            txtPrecio.setText(String.format("%.2f", precioTotal)+" €");
            
            
            saldo -= precio;
            txtSaldo.setText(String.format("%.2f",saldo)+" €");
        } else {
            
            Alert alerta = new Alert(Alert.AlertType.ERROR);
            alerta.setTitle("Error");
            alerta.setContentText("No tienes saldo suficiente para comprar este café.");
            alerta.showAndWait();
        }
    } catch (NumberFormatException e) {
        Alert alerta = new Alert(Alert.AlertType.ERROR);
        alerta.setTitle("Error");
        alerta.setContentText("Algo no ha ido bien...");
        alerta.showAndWait();
    }
}

@FXML
void cancelarCafé(ActionEvent event) {
    Café caféSeleccionado = (Café) tabla.getSelectionModel().getSelectedItem();
    
    if (caféSeleccionado != null) {
        double importeCafé = caféSeleccionado.getPrecio();
        
        saldo += importeCafé;
        txtSaldo.setText(String.format("%.2f", saldo)+" €");
        
        cafés.remove(caféSeleccionado);
    } else {
        Alert alerta = new Alert(Alert.AlertType.ERROR);
        alerta.setTitle("Error");
        alerta.setContentText("Selecciona un café de la tabla para cancelar.");
        alerta.showAndWait();
    }
}



    
@FXML
public void recargarSaldo(ActionEvent event) {
    try {
        double cantidadRecargar = Double.parseDouble(txtRecargarCantidad.getText());


        if (cantidadRecargar >= 0) {
            saldo += cantidadRecargar;
            txtSaldo.setText(String.format("%.2f", saldo)+" €");
        } else {
            Alert alerta = new Alert(Alert.AlertType.ERROR);
            alerta.setTitle("Error");
            alerta.setContentText("La cantidad de recarga no puede ser negativa.");
            alerta.showAndWait();
        }
    } catch (NumberFormatException e) {
        Alert alerta = new Alert(Alert.AlertType.ERROR);
        alerta.setTitle("Error");
        alerta.setContentText("Debe ingresar un número válido para recargar el saldo.");
        alerta.showAndWait();
    }
}

    
    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.txtCantidad.setText("1");
        
        
        
        
        tg = new ToggleGroup();
        btnCapu.setToggleGroup(tg);
        btnLatte.setToggleGroup(tg);
        btnCortado.setToggleGroup(tg);
        
        txtSaldo.setEditable(false);
        txtPrecio.setEditable(false);
        
        cafés = FXCollections.observableArrayList();
        filtroCafés = FXCollections.observableArrayList();
        
        ObservableList<String> opciones = FXCollections.observableArrayList("Pequeño","Mediano","Grande");
        btnTamanio.setItems(opciones);
        
        this.colTipo.setCellValueFactory(new PropertyValueFactory("tipo"));
        this.colTamanio.setCellValueFactory(new PropertyValueFactory("tamaño"));
        this.colCantidad.setCellValueFactory(new PropertyValueFactory("cantidad"));
        this.colPrecio.setCellValueFactory(new PropertyValueFactory("precio"));
        
        
    }

   

}

