/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cafe;

/**
 *
 * @author Mañana_pos3
 */
public class Café {
    
    private String tipo,tamaño;
    private int cantidad;
    private double precio;
    
    public Café(String tipo, String tamaño, int cantidad, double precio) {
    this.tipo = tipo;
    this.tamaño = tamaño;
    this.cantidad = cantidad;
    this.precio = precio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
    
    
    
}
